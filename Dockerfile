FROM python:3.9-slim-buster

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN pip3 install --upgrade pip wheel setuptools \
 && pip3 install -r requirements.txt

COPY server server

# Launch
CMD python3 -m server

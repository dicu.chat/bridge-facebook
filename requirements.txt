aiohttp==3.7.4
sanic==21.6.0
python-dotenv==0.18.0
django-environ==0.4.5
